import Formulario from "./Formulario"
import Result from "./Result"
import Loading from "./Loading"
import useWeather from "../hooks/useWeather"

const AppWeather = () => {

  const { result, loading, noResult }= useWeather(); 

  console.log(loading)

  return (
    <>
        <main className="dos-columnas">
            <Formulario />

            {loading ? <Loading /> : 
              result?.name ? <Result /> :
              noResult ? <p>{noResult}</p>
            : <p>The weather is going to show here</p>}
            
        </main>
    </>
  )
}

export default AppWeather
