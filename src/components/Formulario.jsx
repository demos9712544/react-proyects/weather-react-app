import { useState } from "react"
import useWeather from "../hooks/useWeather" 


const Formulario = () => {

    const [ alert, setAlert ] = useState('')
    const { search, dataSearch, getWeather } = useWeather() 
    const { city, country } = search

    const handleSubmit = e => {
        e.preventDefault()

        if(Object.values(search).includes('')) {
            setAlert('All the field are required')
            return
        }
        setAlert('')
        getWeather(search)
    }

  return (
      <div className="contenedor">
        { alert && <p> { alert } </p> }
        <form
            onSubmit={handleSubmit}
        >
            <div className="campo">
                <label htmlFor="city">City</label>
                <input 
                    type="text"
                    id="city"
                    name="city"
                    onChange={ dataSearch }
                    value={ city }
                />
            </div>
            <div className="campo">
                <label htmlFor="country">Country</label>
                <select 
                    name="country" 
                    id="country"
                    onChange={ dataSearch }
                    value={ country }
                >
                    <option value="Select a country"></option>
                    <option value="US">United State</option>
                    <option value="MX">Mexico</option>
                    <option value="AR">Argentina</option>
                    <option value="AU">Australia</option>
                    <option value="NZ">New Zeland</option>
                    <option value="COL">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">Spain</option>
                    <option value="PR">Peru</option>
                    <option value="PR">Peru</option>
                </select>
            </div>

            <input 
                type="submit"
                value='Check weather'    
            />
        </form>
      </div>
  )
}

export default Formulario
