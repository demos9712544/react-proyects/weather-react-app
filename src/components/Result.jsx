import useWeather from "../hooks/useWeather"


const Result = () => {

    const { result } = useWeather()
    const { name, main, weather } = result;
    console.log(result)

    //Kelvin grades
    const kelvin = 273.15

  return (
    <div className="contenedor weather">
        <h3>The weather in {name } is: </h3>

          <h6 className="weather-description">{ weather[0].description }</h6>
        <p>
           { parseInt(main.temp - kelvin) } <span> &#x2103; </span>
        </p>
        <div className="temp_max_min">
          <p>
              Min: { parseInt(main.temp_min - kelvin) }<span>&#x2103; </span>
          </p>
          <p>
              Max: { parseInt(main.temp_max - kelvin) }<span>&#x2103; </span>
          </p>
          <p>
              Humidity: { parseInt(main.humidity) }<span>%</span>
          </p>
        </div>
    </div>
  )
}

export default Result
